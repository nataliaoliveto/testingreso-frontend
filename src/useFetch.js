import React from 'react'

const useFetch = ( selected ) => {
    
    const [movies, setMovies] = React.useState();
    const [status, setStatus] = React.useState("idle");

    React.useEffect(() => {
        setStatus("loading")
        if(selected !== undefined){
            fetch(`http://localhost:8080/api/home?title=${selected}`)
            .then((response) => response.json()
            .then((data) => {
                if(data === null){
                    setStatus("error");
                } else {
                    setMovies(data);
                    setStatus("success");
                }
            }))
            .catch(error => setStatus("error"))
        }else{
            setMovies([]);
        }
    }, [selected]);

    return { movies , status }
}

export default useFetch;