import './styles.css';
import React from 'react';
import useFetch from './useFetch.js';
import styled from "styled-components";
import Header from './Header';

function App() {

  const [input, setInput] = React.useState("");
  const [selected, setSelected] = React.useState();
  const [back, setBack] = React.useState(true);
  const { movies, status } = useFetch(selected);  

  function handleInputChange(e) {
    setInput(e.target.value);
  }

  function handleSearchClick() {
    setSelected(input)    
    setBack(false)
  }

  function handleBack() {
    setInput("")
    setBack(true)
  }

  if (status === "error" && back === false) {
    return (
      <StateMessage>
        <Header />
        <div>
          <h2>An error ocurred. Please try again</h2>
        </div>
        <ButtonsWrapper>
          <button onClick={() => handleBack()}>Find another movie</button>
        </ButtonsWrapper>
      </StateMessage>
    )
  } else if (back === true) {
    return (
      <div className="App">
        <Header />
        <Input
          placeholder="type a title"
          value={input}
          onChange={handleInputChange}
          type="text" />
        <ButtonsWrapper>
          <button disabled={!input} onClick={() => handleSearchClick()}>Search</button>
        </ButtonsWrapper>
      </div>
    );
  } else if (movies && status === "success") {
      return (
        <div className="App">
          <Header />
          <Table>
            <table>
              <thead>
                <tr>
                  <th>Titles</th>
                </tr>
              </thead>
              <tbody>
                {movies.length === 0 ? <tr><td>No movies with "{selected}"</td></tr> : movies.map((title, i) => <tr key={i}><td>{title}</td></tr>)}                    
              </tbody>
            </table>
          </Table>
          <ButtonsWrapper>
            <button onClick={() => handleBack()}>Find another movie</button>
          </ButtonsWrapper>
        </div>
      );   
  } else if (status === "loading") {
    return (
      <StateMessage>
        <Header />
        <div>
          <h2>Loading...</h2>
        </div>
      </StateMessage>
    )
  } 
}

export default App;

const Table = styled.div`
overflow-x:auto;

table{
  margin: auto;
  background: #313640;
  border-radius: 10px;
  align-items: center;  
  justify-content: center;
  color: grey;
  padding: 20px;
  border: 1px solid #3ED3D1;
  min-width: 25%;


  th {
    color: white;
    display: flex;
    border-bottom: 1px solid #FC833F;
    margin-bottom: 20px;
    justify-content: center;
    font-size: 1.5em;
  }

  td {
    color: #F7F7F7;
    padding: 5px;
    font-size: 1em;
    border-bottom: 1px solid #3ED3D1;
  }
}
`

const StateMessage = styled.div`

  margin: auto;
  font-size: 20px;
  color: #3ED3D1;
  text-align: center;
  background-color: none;
  animation-name: fadein;
  animation-duration: 0.5s;

  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

`

const ButtonsWrapper = styled.div`

    button {
        background-color: #FC833F;
        color: #fff;
        border: 0;
        padding: 5px;
        border-radius: 5px;
        cursor: pointer;
        font-size: 1em;
        margin: 30px;
        text-align: center;  
        cursor: pointer;
        transition: all ease-out 0.1s;
        width: 200px;
        height: 50px;

        &:hover {
        filter: brightness(1.20);
        transform: translateY(2px);        
        font-weight: 600;
        }

        &:disabled{
        background-color: grey;
        filter: none;
        transform: none;
        text-decoration-line: line-through;
        cursor: default;
        }
    }
`

const Input = styled.input`

    margin-bottom: 10px;
    box-shadow: 1px 3px 5px rgba(0,0,0,0.1);         
    border: 0px transparent;
    border-radius: 5px;
    height: 50px;
    width: 300px;
    font-size: 0.75em;
    margin-top: 50px;

    &::placeholder {
        color: grey;
        text-align: center;
    }

    &:focus{
        outline: solid;
        outline-width: 1px;
        outline-color: #FC833F;
    }
  }
`