import React from 'react';
import './styles.css';

const Header =() =>{
    return(
        <header className="App-header">
          <img src="/icon.png" className="App-logo" alt="logo" />
          <h1>
            M o v i e s
          </h1>
        </header>
    )
}

export default Header;